﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Preverjanje_Enoroki_jack
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int kredit = 500;
        int igre = 0;
        int igraniKovanci = 0;
        int osvojeniKovanci = 0;
        int stKovancev = 0;

        private void button_igraj_Click(object sender, EventArgs e)
        {
            if (kredit > 0)
            {
                stKovancev = Convert.ToInt32(numericUpDown1.Value);
                kredit -= stKovancev;
                label_kredit.Text = kredit.ToString();
                igre++;
                label_igre.Text = igre.ToString();
                igraniKovanci += stKovancev;
                label_igraniKovanci.Text = igraniKovanci.ToString();

                Random naklj = new Random();
                int st1 = naklj.Next(0, 10);
                int st2 = naklj.Next(0, 10);
                int st3 = naklj.Next(0, 10);

                textBox1.Text = st1.ToString();
                textBox1.Text += st2.ToString();
                textBox1.Text += st3.ToString();

                if (st1 == st2 && st1 == st3)
                {
                    if (textBox1.Text == "000")
                    {
                        switch (stKovancev)
                        {
                            case 1: kredit += 1; break;
                            case 2: kredit += 2; break;
                            case 3: kredit += 3; break;
                        }
                    }
                    else if (textBox1.Text == "111")
                    {
                        switch (stKovancev)
                        {
                            case 1: kredit += 2; break;
                            case 2: kredit += 4; break;
                            case 3: kredit += 6; break;
                        }
                    }
                    else if (textBox1.Text == "222")
                    {
                        switch (stKovancev)
                        {
                            case 1: kredit += 3; break;
                            case 2: kredit += 6; break;
                            case 3: kredit += 9; break;
                        }
                    }
                    else if (textBox1.Text == "333")
                    {
                        switch (stKovancev)
                        {
                            case 1: kredit += 4; break;
                            case 2: kredit += 8; break;
                            case 3: kredit += 12; break;
                        }
                    }
                    else if (textBox1.Text == "444")
                    {
                        switch (stKovancev)
                        {
                            case 1: kredit += 5; break;
                            case 2: kredit += 10; break;
                            case 3: kredit += 15; break;
                        }
                    }
                    else if (textBox1.Text == "555")
                    {
                        switch (stKovancev)
                        {
                            case 1: kredit += 6; break;
                            case 2: kredit += 12; break;
                            case 3: kredit += 18; break;
                        }
                    }
                    else if (textBox1.Text == "666")
                    {
                        switch (stKovancev)
                        {
                            case 1: kredit += 7; break;
                            case 2: kredit += 14; break;
                            case 3: kredit += 21; break;
                        }
                    }
                    else if (textBox1.Text == "777")
                    {
                        switch (stKovancev)
                        {
                            case 1: kredit += 8; break;
                            case 2: kredit += 16; break;
                            case 3: kredit += 24; break;
                        }
                    }
                    else if (textBox1.Text == "888")
                    {
                        switch (stKovancev)
                        {
                            case 1: kredit += 9; break;
                            case 2: kredit += 18; break;
                            case 3: kredit += 27; break;
                        }
                    }
                    else if (textBox1.Text == "999")
                    {
                        switch (stKovancev)
                        {
                            case 1: kredit += 10; break;
                            case 2: kredit += 20; break;
                            case 3: kredit += 30; break;
                        }
                    }

                    label_kredit.Text = kredit.ToString();

                    osvojeniKovanci += stKovancev;
                    label_osvojeniKovanci.Text = osvojeniKovanci.ToString();
                }
                if (kredit <= 0)
                    MessageBox.Show("Konec igre. :-( Začni znova");
            }
        }

        private void button_novo_Click(object sender, EventArgs e)
        {
            kredit = 500;
            label_kredit.Text = kredit.ToString();
            igre = 0;
            label_igre.Text = igre.ToString();
            igraniKovanci = 0;
            label_igraniKovanci.Text = igraniKovanci.ToString();
            osvojeniKovanci = 0;
            label_osvojeniKovanci.Text = osvojeniKovanci.ToString();
            textBox1.Text = "";
        }

        private void button_izhod_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
