﻿namespace Preverjanje_Enoroki_jack
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label_igre = new System.Windows.Forms.Label();
            this.label_igraniKovanci = new System.Windows.Forms.Label();
            this.label_osvojeniKovanci = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.button_igraj = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button_novo = new System.Windows.Forms.Button();
            this.button_izhod = new System.Windows.Forms.Button();
            this.label_kredit = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Preverjanje_Enoroki_jack.Properties.Resources.Vrednosti_kovancev;
            this.pictureBox1.Location = new System.Drawing.Point(13, 13);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(254, 228);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("OCR A Std", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(286, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(365, 68);
            this.label1.TabIndex = 1;
            this.label1.Text = "Kredit:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("OCR A Std", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(286, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(493, 39);
            this.label2.TabIndex = 1;
            this.label2.Text = "Št. igranih iger:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("OCR A Std", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(286, 120);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(605, 39);
            this.label3.TabIndex = 1;
            this.label3.Text = "Št. igranih kovancev:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("OCR A Std", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(286, 159);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(493, 39);
            this.label4.TabIndex = 1;
            this.label4.Text = "Osvojeni kovanci:";
            // 
            // label_igre
            // 
            this.label_igre.AutoSize = true;
            this.label_igre.Font = new System.Drawing.Font("OCR A Std", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_igre.ForeColor = System.Drawing.Color.Red;
            this.label_igre.Location = new System.Drawing.Point(931, 81);
            this.label_igre.Name = "label_igre";
            this.label_igre.Size = new System.Drawing.Size(44, 39);
            this.label_igre.TabIndex = 2;
            this.label_igre.Text = "0";
            // 
            // label_igraniKovanci
            // 
            this.label_igraniKovanci.AutoSize = true;
            this.label_igraniKovanci.Font = new System.Drawing.Font("OCR A Std", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_igraniKovanci.ForeColor = System.Drawing.Color.Red;
            this.label_igraniKovanci.Location = new System.Drawing.Point(931, 120);
            this.label_igraniKovanci.Name = "label_igraniKovanci";
            this.label_igraniKovanci.Size = new System.Drawing.Size(44, 39);
            this.label_igraniKovanci.TabIndex = 2;
            this.label_igraniKovanci.Text = "0";
            // 
            // label_osvojeniKovanci
            // 
            this.label_osvojeniKovanci.AutoSize = true;
            this.label_osvojeniKovanci.Font = new System.Drawing.Font("OCR A Std", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_osvojeniKovanci.ForeColor = System.Drawing.Color.Red;
            this.label_osvojeniKovanci.Location = new System.Drawing.Point(931, 159);
            this.label_osvojeniKovanci.Name = "label_osvojeniKovanci";
            this.label_osvojeniKovanci.Size = new System.Drawing.Size(44, 39);
            this.label_osvojeniKovanci.TabIndex = 2;
            this.label_osvojeniKovanci.Text = "0";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.numericUpDown1);
            this.groupBox1.Controls.Add(this.button_igraj);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Location = new System.Drawing.Point(13, 248);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(962, 185);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.numericUpDown1.Location = new System.Drawing.Point(271, 52);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(89, 80);
            this.numericUpDown1.TabIndex = 2;
            this.numericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // button_igraj
            // 
            this.button_igraj.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.button_igraj.Font = new System.Drawing.Font("Segoe UI", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_igraj.Location = new System.Drawing.Point(633, 20);
            this.button_igraj.Name = "button_igraj";
            this.button_igraj.Size = new System.Drawing.Size(225, 134);
            this.button_igraj.TabIndex = 1;
            this.button_igraj.Text = "Igraj";
            this.button_igraj.UseVisualStyleBackColor = false;
            this.button_igraj.Click += new System.EventHandler(this.button_igraj_Click);
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Ravie", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.textBox1.Location = new System.Drawing.Point(366, 19);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(260, 135);
            this.textBox1.TabIndex = 0;
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button_novo
            // 
            this.button_novo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_novo.Font = new System.Drawing.Font("Segoe UI", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_novo.Location = new System.Drawing.Point(284, 439);
            this.button_novo.Name = "button_novo";
            this.button_novo.Size = new System.Drawing.Size(219, 109);
            this.button_novo.TabIndex = 4;
            this.button_novo.Text = "Novo";
            this.button_novo.UseVisualStyleBackColor = false;
            this.button_novo.Click += new System.EventHandler(this.button_novo_Click);
            // 
            // button_izhod
            // 
            this.button_izhod.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_izhod.Font = new System.Drawing.Font("Segoe UI", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_izhod.Location = new System.Drawing.Point(646, 439);
            this.button_izhod.Name = "button_izhod";
            this.button_izhod.Size = new System.Drawing.Size(219, 109);
            this.button_izhod.TabIndex = 4;
            this.button_izhod.Text = "Izhod";
            this.button_izhod.UseVisualStyleBackColor = false;
            this.button_izhod.Click += new System.EventHandler(this.button_izhod_Click);
            // 
            // label_kredit
            // 
            this.label_kredit.AutoSize = true;
            this.label_kredit.Font = new System.Drawing.Font("OCR A Std", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_kredit.ForeColor = System.Drawing.Color.Red;
            this.label_kredit.Location = new System.Drawing.Point(821, 13);
            this.label_kredit.Name = "label_kredit";
            this.label_kredit.Size = new System.Drawing.Size(167, 68);
            this.label_kredit.TabIndex = 2;
            this.label_kredit.Text = "500";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 561);
            this.Controls.Add(this.button_izhod);
            this.Controls.Add(this.button_novo);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label_osvojeniKovanci);
            this.Controls.Add(this.label_igraniKovanci);
            this.Controls.Add(this.label_kredit);
            this.Controls.Add(this.label_igre);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Enoroki jack";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label_igre;
        private System.Windows.Forms.Label label_igraniKovanci;
        private System.Windows.Forms.Label label_osvojeniKovanci;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Button button_igraj;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button_novo;
        private System.Windows.Forms.Button button_izhod;
        private System.Windows.Forms.Label label_kredit;
    }
}

